﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Decromat
{
    class MediaList
    {
        public int Num { get; }
        public String FilePath;
        public int ResourceId;
        public List<Block> Blocks = new List<Block>();
        private static Messages message = Messages.GetInstance();

        public MediaList(string Path, int resourceId)
        {
            FilePath = Path;
            ResourceId = resourceId;
            Parse(FilePath);
        }

        public void SaveToFile(string Filename)
        {
            if (Blocks.Count > 0)
            {
                List<string> incremenable = new List<string>();
                using (System.IO.StreamWriter file = new System.IO.StreamWriter($"{Filename}", false, Encoding.GetEncoding("windows-1251")))
                {
                    foreach (Block Block in Blocks)
                    {
                        foreach (Block.Item item in Block.Items)
                        {
                            file.WriteLine($"T\t{Block.Time}\t\"{item.Path}\"\t{item.TypeOfMedia}");
                        }
                    }
                }
            }
        }

        public void Parse(string Filename)
        {
            string LastTime = "";
            Block CurrentBlock = null;
            using (StreamReader Reader = new StreamReader(Filename, Encoding.GetEncoding("windows-1251")))
            {
                while (!Reader.EndOfStream)
                {
                    string Line = Reader.ReadLine();
                    string[] Fields = Line.Replace("\"", "").Split('\t');
                    string CurrentTime = Fields.GetValue(1).ToString();
                    // if a new string time is differ with a current block time it means a new block begins
                    if (CurrentTime != LastTime)
                    {
                        if (CurrentBlock != null)
                        {
                            // before we should save a current block if it exists 
                            Blocks.Add(CurrentBlock);
                        }
                        // set time of new block as current
                        LastTime = CurrentTime;
                        CurrentBlock = new Block(ResourceId, LastTime);
                    }
                    CurrentBlock.AddItem(Fields);
                }
                Blocks.Add(CurrentBlock);
            }
        }

        public class Block
        {
            public int Num { get; set; }
            public int Duration { get; set; }
            public int SecUsed { get; set; }
            public int SecFree { get; set; } 
            public int ResourceId { get; set; }
            public string Time { get; set; }
            public List<Item> Items = new List<Item>();

            public Block(int resourceId, string time)
            {
                ResourceId = resourceId;
                Duration = GetPlanBlockDuration(resourceId, time);
                SecFree = Duration;
                Time = time;
            }

            public void AddItem(string[] ItemParams)
            {
                Item NewItem = new Item(ItemParams);
                Items.Add(NewItem);
                AddUsedDuration(NewItem.Duration);
            }
            
            private void AddUsedDuration(int AddUsedDuration)
            {
                SecUsed += AddUsedDuration;
                SecFree -= AddUsedDuration;
            }

            private int GetPlanBlockDuration(int resourceId, string time)
            {
                DB database = DB.GetInstance();
                string query = "SELECT BlockDuration " +
                    "FROM ResourceBlockDurations " +
                    $"WHERE ResourceBlockDurations.ResourceId = { resourceId } " +
                    $"AND ResourceBlockDurations.BlockTime = '{ time }' " ;
                List<List<Object>> Result = database.Read(query);
                if (Result.Count > 0 && Result[0].Count > 0)
                {
                    return Convert.ToInt16(Result[0][0]);
                }
                else
                {
                    // TODO refactor
                    message.AddError($"Не получилось извлечь планируемую длину блока для ресурса {resourceId} на время {time}");
                    return 0;
                }
            }


            public class Item
            {
                // this is info we got from schedule file
                // TypeOfItem is action raises in djinn (like "play on time")
                // TypeOfMedia is type of media we got (like ad, music)
                public string TypeOfItem { get; }
                public string TypeOfMedia { get; }
                public int Hour { get; }
                public int Minute { get; }
                public int Second { get; }
                public int Duration { get; }
                public string Path { get; }

                public Item(string[] ItemString)
                {
                    List<int> SpTime = SplitTime(ItemString[1]);
                    TypeOfItem = ItemString[0];
                    TypeOfMedia = ItemString[3];
                    Path = ItemString[2];
                    Hour = SpTime[0];
                    Minute = SpTime[1];
                    Second = SpTime[2];
                    Duration = GetMediaDuration(Path);
                }

                public int GetMediaDuration(string Filename)
                {
                    DB database = DB.GetInstance();
                    string query = "SELECT Duration " +
                        "FROM Mp3Files " +
                        $"WHERE Mp3Files.Filename = '{Filename}'";
                    object Result = database.ReadOne(query);
                    int result = Convert.ToInt16(Result);
                    if (result == 0)
                    {
                        message.AddError($"Медиафайл имеет нулевую длительность, либо его длительность не указана в базе. Файл {Filename}");
                    }
                    return result;
                }

                public string GetMediaType(string filename)
                {
                    DB database = DB.GetInstance();
                    string query = "SELECT Type " +
                        "FROM Mp3Files " +
                        $"WHERE Mp3Files.Filename = '{filename}'";
                    List<List<Object>> Result = database.Read(query);
                    return Result[0][0].ToString();
                }

                private List<int> SplitTime(string TimeString)
                {
                    List<int> Time = new List<int>();

                    foreach (string value in TimeString.Split(':'))
                    {
                        Time.Add(Convert.ToInt16(value));
                    }
                    return Time;
                }

                private string JoinTime(string[] Time )
                {
                    string time = String.Join(":", Time);
                    return time;
                }
            }
        }
    }
}
