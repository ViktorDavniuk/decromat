﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Decromat
{

    class Program
    {
        static private Messages messages = Messages.GetInstance();
        static private DB database = DB.GetInstance();
        static public bool terminal = false;
        static public string[][] MediaPathes = new string[][] {
            new string[] { "1", "c:\\audio\\Russkoe\\DecroJingles", "J", "c:\\audio\\DecroJingles" },
            new string[] { "1", "c:\\audio\\Russkoe\\DecroMixes", "M", "c:\\audio\\DecroMixes" },
            new string[] { "1", "c:\\audio\\Russkoe\\DecroSongs", "S", "c:\\audio\\DecroSongs" },
            new string[] { "1", "c:\\audio", "A", "" },
            new string[] { "2", "c:\\audio\\Doroznoe\\jingles_2017", "J", "c:\\audio\\jingles_2017" },
            new string[] { "2", "c:\\audio\\Doroznoe\\decro", "M","c:\\audio\\decro" },
            new string[] { "2", "c:\\audio\\Doroznoe\\dobivki", "P", "c:\\audio\\dobivki" },
            new string[] { "2", "c:\\audio\\Doroznoe\\music", "S", "c:\\audio\\music" },
            new string[] { "2", "c:\\audio", "A", "" }
        };

        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                Console.WriteLine("Debug Console");
                ApplicationArguments Arguments = CLArgumentsParser.Parse(args);
                Execute(Arguments);
            }
            return;
        }

        static void Execute(ApplicationArguments arguments)
        {
            switch (arguments.Action)
            {
                case "help":
                    Help();
                    break;
                case "init":
                    Init();
                    break;
                case "update-durations":
                    UpdateDurations();
                    break;
                case "processing":
                    SchedulesProcessing();
                    break;
                case "add-folders":
                    AddFoldersToBase(arguments.Resources, arguments.Folders, arguments.TypeOfMedia);
                    break;
                case "add-alias":
                    AddAlias(arguments.Folders, arguments.Alias);
                    break;
                case "add-files":
                    AddFilesToBase(arguments.Resources, arguments.Filenames, arguments.TypeOfMedia);
                    break;
                default:
                    messages.AddError("Укажите какое-либо действие при помощи ключа --action или -a!");
                    break;
            }
        }

        static void Help()
        {
            string helpText = "Возможные варианты использования программы: " +
                "--action [действие] - указывает, какое именно действие выполнять, например --action update-durations" +
                "Действия: " +
                "           init - первоначальная инициализация приложения, создание базы данных; /n" +
                "           update-durations - обновление медиатеки из папок, указанных в настройках приложения; /n" +
                "           add-folders [--folders \"папка1\", \"папка2\"] [--type тип] [--alias алиас папки в базе (путь, который будет использоваться при выгрузке)] /n" +
                "                        [--resources id ресурсов, для которых будут добавляться файлы из списка] добавление папок в медиатеку; /n" +
                "           add-files[--files \"файл1\", \"файл2\"] [--type тип] {--alias алиас папки в базе (путь, который будет использоваться при выгрузке} /n" +
                "                        [--resources id ресурсов, для которых будут добавляться файлы из списка] добавление файлов в медиатеку; /n" +
                "           add-alias [--folders исходные папки] [--alias алиас] добавление алиаса для папки, исходный путь будет подменяться на алиас при добавлении в базу; /n" +
                "           processing обработка расписаний; /n";
            messages.AddMessage(helpText);
            Console.ReadLine();
        }

        static void Init()
        {
            database.FirstInit();
        }

        static void AddAlias(List<string> pathes, string alias) {
            foreach (string path in pathes)
            {
                DataManipulation.AddAlias(path, alias);
            }
        }

        static void AddFoldersToBase(List<int> resources, List<string> dirs, string type = "A")
        {
            foreach(int resource in resources)
            {
                foreach (string dir in dirs) {
                    DataManipulation.AddMp3Folder(resource, dir, type);
                }
            }
        }

        public static void UpdateDurations()
        {
            foreach (string[] PathData in MediaPathes)
            {
                DataManipulation.AddMp3Folder(Int32.Parse(PathData[0]), PathData[1], PathData[2], PathData[3]);
            }
        }

        static void AddFilesToBase(List<int> resources, List<string> filenames, string type = "A")
        {
            foreach (int resource in resources)
            {
                foreach (string filename in filenames)
                {
                    DataManipulation.AddMp3File(resource, filename, type);
                }
            }
        }

        static public void SchedulesProcessing()
        {
            string Query = "SELECT id, SchedulesPath, FileMask " +
                "FROM Resources;";
            List<List<Object>> Resources = database.Read(Query);
            foreach (var resource in Resources)
            {
                string[] files = Directory.GetFiles(resource[1].ToString(), resource[2].ToString());
                foreach (var file in files)
                {
                    MediaList CurrentList = new MediaList(file, Convert.ToInt32(resource[0]));
                    MediaList UpdatedList = Filler.Manage(CurrentList);
                    UpdatedList.SaveToFile(file); //+ "d");
                }
            }
        }
    }
}
