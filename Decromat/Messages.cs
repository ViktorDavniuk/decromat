﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Decromat
{
    class Messages
    {
        private static Messages instance;
        private static List<string[]> MessagesList = new List<string[]>();

        private Messages()
        { }

        public static Messages GetInstance()
        {
            if (instance == null)
            {
                instance = new Messages();
            }
            return instance;
        }

        public bool AddError(string Message)
        {
            return Add(Message, "error");
        }

        public bool AddMessage(string Message)
        {
            return Add(Message, "message");
        }

        private bool Add(string Message, string Type)
        {
            try
            {
                string[] MessageArr = new string[] { Message, Type };
                Console.WriteLine(Message);
                MessagesList.Add(MessageArr);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<string[]> GetMessages(string Type ="")
        {
            if (Type == "")
            {
                return MessagesList;
            }
            else
            {
                List<string[]> SelectedMessages = new List<string[]>(from message in MessagesList
                                                                     where message[1] == Type
                                                                     select message);
                return SelectedMessages;
            }
        }
    }
}
