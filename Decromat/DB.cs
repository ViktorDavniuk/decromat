﻿using System;
using System.Data.SQLite;
using System.Collections.Generic;
using System.Diagnostics;

namespace Decromat
{
    // implement singletone to use as database provider
    class DB
    {
        private static DB instance;
        private static string CurrentDirectory = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        private static String DbFile = $"{CurrentDirectory}\\data.sqlite3";
        private static Messages message = Messages.GetInstance();

        private DB()
        { }

        public static DB GetInstance()
        {
            if (instance == null)
            {
                instance = new DB();
            }
            return instance;
        }

        public void FirstInit()
        {
            // creates tables if not exist

            String QueryText = "CREATE TABLE IF NOT EXISTS Mp3Files (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "Resource INTEGER, " +
                "Filename TEXT, " +
                "Path TEXT, " +
                "Duration INTEGER, " +
                "Type TEXT, " +
                "CountUsed INTEGER, " +
                "Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                "CONSTRAINT res_file UNIQUE(Resource, Filename)); ";

            QueryText += "CREATE TABLE IF NOT EXISTS Schemes (" +
                 "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                 "Resource INTEGER, " +
                 "Scheme TEXT, " +
                 "Priority INTEGER, " +
                 "CountUsed INTEGER, " +
                 "CONSTRAINT res_scheme UNIQUE(Resource, Scheme)); ";

            QueryText += "CREATE TABLE IF NOT EXISTS Resources (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "Name TEXT, " +
                "Prefix TEXT, " +
                "SchedulesPath TEXT, " +
                "FileMask TEXT, " +
                "CONSTRAINT name_path UNIQUE(Name, SchedulesPath)); ";

            QueryText += "CREATE TABLE IF NOT EXISTS ResourceBlockDurations (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "ResourceId INTEGER, " +
                "BlockTime TEXT, " +
                "BlockDuration INTEGER, " +
                "CONSTRAINT res_blocktime UNIQUE(ResourceId, BlockTime)); ";

            QueryText += "CREATE TABLE IF NOT EXISTS Counter (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "ResourceId INTEGER, " +
                "Filename TEXT, " +
                "Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP); ";

            QueryText += "CREATE TABLE IF NOT EXISTS Alias (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "Path TEXT, " +
                "Destination TEXT, " +
                "Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP," +
                "CONSTRAINT path UNIQUE(Path)); ";
           

            // adds basic info 

            QueryText += "INSERT OR REPLACE INTO Resources (" +
                "Name, " +
                "Prefix, " +
                "SchedulesPath, " +
                "FileMask) " +
                "VALUES " +
                "('Russkoe', " +
                "'RUS', " +
                "'C:\\AIR\\Russkoe', " +
                "'????-??-??.txt'); ";

            QueryText += "INSERT OR REPLACE INTO Resources (" +
               "Name, " +
               "Prefix, " +
               "SchedulesPath, " +
               "FileMask) " +
               "VALUES " +
               "('Doroznoe', " +
               "'DOR', " +
               "'C:\\AIR\\Doroznoe', " +
               "'????-??-??.txt'); ";

            QueryText += "INSERT OR REPLACE INTO Schemes (" +
           "Resource, " +
           "Scheme, " +
           "Priority, " +
           "CountUsed) " +
           "VALUES " +
           "('1', 'J', '1', '0'), " +
           "('1', 'S', '1', '0'), " +
           "('1', 'M', '1', '0'), " +
           "('1', 'JM', '2', '0'), " +
           "('1', 'SJ', '2', '0'), " +
           "('1', 'MSJ', '3','0'), " +
           "('2', 'J', '1', '0'), " +
           "('2', 'JP', '2', '0'), " +
           "('2', 'M', '1', '0'), " +
           "('2', 'MP', '2', '0'), " +
           "('2', 'S', '1', '0'), " +
           "('2', 'P', '2', '0'), " +
           "('2', 'SP', '2', '0'), " +
           "('2', 'MJ', '2', '0'), " +
           "('2', 'PMJ', '3', '0'), " +
           "('2', 'SJ', '1', '0'), " +
           "('2', 'PSJ', '2', '0'), " +
           "('2', 'SS', '3', '0'), " +
           "('2', 'SPS', '3', '0'), " +
           "('2', 'MPSJ', '3', '0'), " +
           "('2', 'MSJ', '2', '0'); ";

            QueryText += "INSERT OR REPLACE INTO ResourceBlockDurations (" +
              "ResourceId, " +
              "BlockTime, " +
              "BlockDuration) " +
              "VALUES " +
              "('2', " +
              "'00:15:00', " +
              "180), " +
              "('2', " +
              "'01:15:00', " +
              "180), " +
              "('2', " +
              "'02:15:00', " +
              "180), " +
              "('2', " +
              "'03:15:00', " +
              "180), " +
              "('2', " +
              "'04:15:00', " +
              "180), " +
              "('2', " +
              "'05:15:00', " +
              "235), " +
              "('2', " +
              "'06:15:00', " +
              "180), " +
              "('2', " +
              "'07:15:00', " +
              "235), " +
              "('2', " +
              "'08:15:00', " +
              "180), " +
              "('2', " +
              "'09:15:00', " +
              "235), " +
              "('2', " +
              "'10:15:00', " +
              "180), " +
              "('2', " +
              "'11:15:00', " +
              "235), " +
              "('2', " +
              "'12:15:00', " +
              "180), " +
              "('2', " +
              "'13:15:00', " +
              "180), " +
              "('2', " +
              "'14:15:00', " +
              "180), " +
              "('2', " +
              "'15:15:00', " +
              "180), " +
              "('2', " +
              "'16:15:00', " +
              "180), " +
              "('2', " +
              "'17:15:00', " +
              "235), " +
              "('2', " +
              "'18:15:00', " +
              "180), " +
              "('2', " +
              "'19:15:00', " +
              "235), " +
              "('2', " +
              "'20:15:00', " +
              "180), " +
              "('2', " +
              "'21:15:00', " +
              "235), " +
              "('2', " +
              "'22:15:00', " +
              "180), " +
              "('2', " +
              "'23:15:00', " +
              "180), " +
              "('1', " +
              "'00:18:00', " +
              "180), " +
              "('1', " +
              "'01:18:00', " +
              "180), " +
              "('1', " +
              "'02:18:00', " +
              "180), " +
              "('1', " +
              "'03:18:00', " +
              "180), " +
              "('1', " +
              "'04:18:00', " +
              "180), " +
              "('1', " +
              "'05:18:00', " +
              "180), " +
              "('1', " +
              "'06:18:00', " +
              "180), " +
              "('1', " +
              "'07:18:00', " +
              "180), " +
              "('1', " +
              "'08:18:00', " +
              "180), " +
              "('1', " +
              "'09:18:00', " +
              "180), " +
              "('1', " +
              "'10:18:00', " +
              "180), " +
              "('1', " +
              "'11:18:00', " +
              "180), " +
              "('1', " +
              "'12:18:00', " +
              "180), " +
              "('1', " +
              "'13:18:00', " +
              "180), " +
              "('1', " +
              "'14:18:00', " +
              "180), " +
              "('1', " +
              "'15:18:00', " +
              "180), " +
              "('1', " +
              "'16:18:00', " +
              "180), " +
              "('1', " +
              "'17:18:00', " +
              "180), " +
              "('1', " +
              "'18:18:00', " +
              "180), " +
              "('1', " +
              "'19:18:00', " +
              "180), " +
              "('1', " +
              "'20:18:00', " +
              "180), " +
              "('1', " +
              "'21:18:00', " +
              "180), " +
              "('1', " +
              "'22:18:00', " +
              "180), " +
              "('1', " +
              "'23:18:00', " +
              "180), " +
              "('1', " +
              "'00:48:00', " +
              "270), " +
              "('1', " +
              "'01:48:00', " +
              "270), " +
              "('1', " +
              "'02:48:00', " +
              "270), " +
              "('1', " +
              "'03:48:00', " +
              "270), " +
              "('1', " +
              "'04:48:00', " +
              "270), " +
              "('1', " +
              "'05:48:00', " +
              "270), " +
              "('1', " +
              "'06:48:00', " +
              "270), " +
              "('1', " +
              "'07:48:00', " +
              "270), " +
              "('1', " +
              "'08:48:00', " +
              "270), " +
              "('1', " +
              "'09:48:00', " +
              "270), " +
              "('1', " +
              "'10:48:00', " +
              "270), " +
              "('1', " +
              "'11:48:00', " +
              "270), " +
              "('1', " +
              "'12:48:00', " +
              "270), " +
              "('1', " +
              "'13:48:00', " +
              "270), " +
              "('1', " +
              "'14:48:00', " +
              "270), " +
              "('1', " +
              "'15:48:00', " +
              "270), " +
              "('1', " +
              "'16:48:00', " +
              "270), " +
              "('1', " +
              "'17:48:00', " +
              "270), " +
              "('1', " +
              "'18:48:00', " +
              "270), " +
              "('1', " +
              "'19:48:00', " +
              "270), " +
              "('1', " +
              "'20:48:00', " +
              "270), " +
              "('1', " +
              "'21:48:00', " +
              "270), " +
              "('1', " +
              "'22:48:00', " +
              "270), " +
              "('1', " +
              "'23:48:00', " +
              "270); ";

            Exec(QueryText);
        }

        public void Exec(string query)
        {
            System.Diagnostics.Debug.WriteLine(DbFile);
            using (SQLiteConnection connection = new SQLiteConnection($"Data Source={DbFile}"))
            {
                using (SQLiteCommand command = new SQLiteCommand(query, connection)) {
                    connection.Open();
                     int f = command.ExecuteNonQuery();
                }
            }
        }

        public List<List<Object>> Read(string query)
        {
            using (SQLiteConnection connection = new SQLiteConnection($"Data Source={DbFile}"))
            {
                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    List<List<Object>> Result = new List<List<Object>>();
                    connection.Open();
                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        List<Object> Row = new List<Object>();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            Row.Add(reader[i]);
                        }
                        Result.Add(Row);
                    }
                    return Result;
                }
            }
        }

        public Object ReadOne(string query)
        {
            List<List<Object>> Result = Read(query);
            if (Result.Count > 0 && Result[0].Count > 0)
            {
                return Result[0][0];
            }
            else
            {
                return null;
            }
        }
    }
}
