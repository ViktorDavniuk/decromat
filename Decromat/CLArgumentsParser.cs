﻿using System.Collections.Generic;
using Fclp;

namespace Decromat
{
    class ApplicationArguments
    {
        public string Action { get; set; }
        public List<string> Folders { get; set; }
        public List<string> Filenames { get; set; }
        public List<int> Resources { get; set; }
        public string Alias { get; set; }
        public string TypeOfMedia { get; set; }

    }

    class CLArgumentsParser
    {
        public static ApplicationArguments Parse(string[] args)
        {
            ApplicationArguments arguments = new ApplicationArguments();
            var parser = new FluentCommandLineParser<ApplicationArguments>();
            var p = new FluentCommandLineParser();
            parser.Setup(arg=>arg.Action).As('a', "action");
            parser.Setup(arg=>arg.Alias).As('l', "alias");
            parser.Setup(arg => arg.TypeOfMedia).As('t', "type");
            p.Setup<List<string>>('f', "filenames").Callback(items => parser.Object.Filenames = items);
            p.Setup<List<string>>('p', "folders").Callback(items => parser.Object.Folders = items);
            p.Setup<List<int>>('r', "resources").Callback(items => parser.Object.Resources = items);
            var result = parser.Parse(args);
            var result2 = p.Parse(args);
            if (result.HasErrors == false && result2.HasErrors == false) {
                return parser.Object;
            } else
            {
                return new ApplicationArguments();
            }
        }
    }
}
