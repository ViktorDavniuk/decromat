﻿using System.Collections.Generic;

namespace Decromat
{
    class Counter
    {
        static DB database = DB.GetInstance();
        public static void IncMp3File(int resourceId, List<string> mediafiles)
        {
            string mediafilesQuery = "";
            foreach (string path in mediafiles) {
                mediafilesQuery += $"'{path}', ";
            }
            string Query = $"UPDATE Mp3Files SET CountUsed = CountUsed + 1 WHERE Filename in ({mediafilesQuery.Remove(mediafilesQuery.Length-2)}) AND Resource = {resourceId}";
            database.Exec(Query);
        }
    }
}
