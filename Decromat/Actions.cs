﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Decromat
{
    class Actions
    {
        static private Settings MySettings = Settings.GetInstance();
        static private Messages messenger = Messages.GetInstance();
        static private DB database = DB.GetInstance();
        static public string[][] MediaPathes = new string[][] {
            new string[] { "1", "c:\\audio\\Russkoe\\DecroJingles", "J" },
            new string[] { "1", "c:\\audio\\Russkoe\\DecroMixes", "M" },
            new string[] { "1", "c:\\audio\\Russkoe\\DecroSongs", "S" },
            new string[] { "1", "c:\\audio", "A" },
            new string[] { "2", "c:\\audio\\Doroznoe\\jingles_2017", "J" },
            new string[] { "2", "c:\\audio\\Doroznoe\\decro", "M" },
            new string[] { "2", "c:\\audio\\Doroznoe\\dobivki", "P" },
            new string[] { "2", "c:\\audio\\Doroznoe\\music", "S" },
            new string[] { "2", "c:\\audio", "A" }
        };

        static void Soso(string[] args)
        {
            Int32 ArrLength = args.Length;
            if (ArrLength > 0)
            {
                List<String> arguments = args.ToList();
                Execute(arguments);
            }
        }

        static void Execute(List<String> arguments)
        {
            switch (arguments[0])
            {
                case "help":
                    Help();
                    break;
                case "-i":
                    Init();
                    break;
                case "-ud":
                    UpdateDurations();
                    break;
                case "-p":
                    SchedulesProcessing();
                    break;
                case "-udi":
                    UpdateDurationsIn(Int32.Parse(arguments[1]), arguments[2], arguments[3]);
                    break;
                case "-a":
                    AddFileToBase(Int32.Parse(arguments[1]), arguments[2], arguments[3]);
                    break;
                default:
                    break;
            }
        }

        static void Help()
        { }

        static void Init()
        {
            database.FirstInit();
        }

        static public void UpdateDurations()
        {
            foreach (string[] PathData in MediaPathes)
            {
                UpdateDurationsIn(Int32.Parse(PathData[0]), PathData[1], PathData[2]);
            }
        }

        static void UpdateDurationsIn(int resource, String dir, String type = "A")
        {
            var files = Directory.EnumerateFiles(dir, "*.mp3");
            foreach (var filename in files)
            {
                DataManipulation.AddMp3File(resource, filename, type);
            }
        }

        static void AddFileToBase(int resource, string filename, string type = "A")
        {
            DataManipulation.AddMp3File(resource, filename, type);
        }

        static void SchedulesProcessing()
        {
            string Query = "SELECT id, SchedulesPath, FileMask " +
                "FROM Resources;";
            List<List<Object>> Resources = database.Read(Query);
            foreach (var resource in Resources)
            {
                string[] files = Directory.GetFiles(resource[1].ToString(), resource[2].ToString());
                foreach (var file in files)
                {
                    MediaList CurrentList = new MediaList(file, Convert.ToInt32(resource[0]));
                    Filler CurrentFiller = new Filler(CurrentList);
                }
            }
        }
    }
}
