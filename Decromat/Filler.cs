﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Decromat
{
    class Filler
    {
    
        static DB database = DB.GetInstance();
        static Messages message = Messages.GetInstance();

        public Filler(MediaList list)
        { }

        public static MediaList Manage(MediaList list)
        {
            string GetSchemesQuery = "SELECT * " +
                "FROM Schemes " +
                "WHERE " +
                $"Schemes.Resource = {list.ResourceId} " +
                $"ORDER BY Priority, random();";

            List<List<Object>> Schemes = database.Read(GetSchemesQuery);
          
            foreach (MediaList.Block CurrentBlock in list.Blocks)
            {
                if (CurrentBlock.SecFree > 0)
                {
                    message.AddMessage($"Блок на {CurrentBlock.Time} имеет свободные {CurrentBlock.SecFree} секунд.");
                    List<List<Object>> ListElements = FindElements(CurrentBlock, Schemes);
                    if (ListElements.Count > 0)
                    {
                        List<Object> Elements = ListElements[0];
                        foreach (string Element in Elements)
                        {
                            string[] El = new string[] { "T", CurrentBlock.Time, Element, "m" };
                            CurrentBlock.AddItem(El);
                            message.AddMessage($"Добавили файл {Element}, осталось {CurrentBlock.SecFree}");
                        }
                    }
                    else
                    {
                        message.AddError($"Не удалось заполнить блок {CurrentBlock.Time} на {CurrentBlock.ResourceId} (заменить) \nДлина блока: {CurrentBlock.SecFree} секунд.");
                    }
                }
            }
            return list;
        }

        private static List<List<Object>> FindElements(MediaList.Block CurrentBlock, List<List<Object>> Schemes)
        {
            foreach (List<Object> Scheme in Schemes)
            {
                List<string> SelectPart = new List<string>();
                List<string> FromPart = new List<string>();
                List<string> WherePart1 = new List<string>();
                List<string> WherePart2 = new List<string>();
                List<string> OrderByPart = new List<string>();
                string SchemeString = Scheme[2].ToString();

                for (int i = 0; i < SchemeString.Length; i++)
                {
                    SelectPart.Add($"File{i}.Filename AS Filename{i}");
                    FromPart.Add($"Mp3Files AS File{i}");
                    WherePart1.Add($"File{i}.Type = '{SchemeString[i]}' AND File{i}.Resource = {CurrentBlock.ResourceId}");
                    WherePart2.Add($"File{i}.Duration");
                    OrderByPart.Add( $"File{i}.CountUsed ASC");
                }
                string SelectPartString = String.Join(", ", SelectPart.ToArray<String>());
                string FromPartString = String.Join(", ", FromPart.ToArray<String>());
                string WherePart1String = String.Join(" AND ", WherePart1.ToArray<String>());
                string WherePart2String = String.Join(" + ", WherePart2.ToArray<String>());
                //string OrderByPartString = String.Join(", ", OrderByPart.ToArray<String>());

                string FindElementsQuery = $"SELECT {SelectPartString} " +
                    $"FROM " +
                    $"{FromPartString} " +
                    $"WHERE " +
                    $"{WherePart1String} " +
                    $"AND ({WherePart2String}) = {CurrentBlock.SecFree} " +
                    $"ORDER BY " +
                   // $"{OrderByPartString} " +
                    $"random() " +
                    $"LIMIT 1; ";
               List<List<Object>> result = database.Read(FindElementsQuery);
               if (result.Count > 0)
               {
                    return result;
               }
            }
            return new List<List<object>>();
        }
    }
}
