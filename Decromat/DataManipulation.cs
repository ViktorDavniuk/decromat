﻿
using System;
using System.IO;
using System.Collections.Generic;
using NAudio.Wave;

namespace Decromat
{
    class DataManipulation
    {
        private static DB database = DB.GetInstance();
        private static Messages message = Messages.GetInstance();

        public static List<List<Object>> GetAllResources(string columns="*") {
        string query = $"SELECT {columns} FROM Resources";
        List<List<Object>> result = DB.GetInstance().Read(query);
            return result;
        }

        public static List<List<Object>> GetAllSchemes(string columns = "*")
        {
            string query = $"SELECT {columns} FROM Schemes";
            List<List<Object>> result = DB.GetInstance().Read(query);
            return result;
        }

        public static List<List<Object>> GetAllMp3Files(string columns = "*")
        {
            string query = $"SELECT {columns} FROM Mp3Files";
            List<List<Object>> result = DB.GetInstance().Read(query);
            return result;
        }

        public static string GetAlias(string path)
        {
            string query = $"SELECT Destination FROM Alias WHERE Path='{path}'";
            object result = database.ReadOne(query);
            if (result == null)
            {
                return path;
            } else {
                try
                {
                    return result.ToString();
                }
                catch (Exception)
                {
                    message.AddError($"Ошибка при определении алиаса для {path}!");
                    return path;
                }
            }
        }

        public static void AddScheme(int resourceId, string scheme, int priority, int count = 0)
        {
            string QueryText = "INSERT INTO Schemes (" +
                "Resource, " +
                "Scheme, " +
                "Priority, " +
                "Count) " +
                "VALUES " +
                $"('{resourceId}', '{scheme}', '{priority}', '{count}') " +
                $"ON CONFLICT(res_scheme) DO UPDATE SET Priority='{priority}', Count='{count}'; ";
            database.Exec(QueryText);
        }

        public static void AddAlias(string path, string destination) {
            string QueryText = "INSERT INTO Alias (" +
            "Path, " +
            "Destination) " +
            "VALUES " +
            $"('{path}', '{destination}') " +
            $"ON CONFLICT(path) DO UPDATE SET Path='{path}', Destination='{destination}'; ";
            database.Exec(QueryText);
        }

        public static void AddResource(string name, string prefix, string schedulesPath, string fileMask)
        {
            string QueryText = "INSERT INTO Resources (" +
                "Name, " +
                "Prefix, " +
                "SchedulesPath, " +
                "FileMask) " +
                "VALUES " +
                $"('{name}', '{prefix}', '{schedulesPath}', '{fileMask}') " +
                $"ON CONFLICT(name_path) DO UPDATE SET Filemask='{fileMask}', Prefix='{prefix}'; ";
            database.Exec(QueryText);
        }

        public static void AddMp3File(int resource, string filename, string type = "A")
        {
            Mp3FileReader re = new Mp3FileReader(filename);
            int duration = Convert.ToInt32(re.TotalTime.TotalSeconds);
            int rate = re.Mp3WaveFormat.SampleRate;
            // TODO REFACTOR!
            string path = Path.GetDirectoryName(filename);
            string decorPath = DataManipulation.GetAlias(path);
            filename = filename.Replace(path, decorPath);
            string Query = "INSERT INTO Mp3Files (" +
                     "Resource, " +
                    "Filename, " +
                    "Path, " +
                    "Duration, " +
                    "Type, " +
                    "CountUsed) " +
                    "VALUES (" +
                    $"\"{resource}\", " +
                    $"\"{filename}\", " +
                    $"\"{path}\", " +
                    $"{duration}, " +
                    $"\"{type}\", " +
                    $"\"0\") " +
                    $"ON CONFLICT(resource, filename) DO UPDATE SET Duration='{duration}', Type='{type}'; ";
            message.AddMessage("Добавлен " + filename);
            try
            {
                database.Exec(Query);
            }
            catch (Exception)
            {
                message.AddError($"Не удалось звписать данные о файле {filename} в базу данных!");
            }
        }

        public static void AddMp3Folder(int resource, string dir, string type = "A", string alias = "")
        {
            var files = Directory.EnumerateFiles(dir, "*.mp3");
            if (alias != "") {
                DataManipulation.AddAlias(dir, alias);
            }

            foreach (var filename in files)
            {
                DataManipulation.AddMp3File(resource, filename, type);
            }
        }
    }
}