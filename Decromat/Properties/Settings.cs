﻿using System;
using System.IO;
using System.Xml;

namespace Decromat
{
    // implement singleton to use as settings provider
    class Settings
    {

        private static Settings instance;
        private static String CurrentDirectory = Directory.GetCurrentDirectory();
        private XmlDocument document = new XmlDocument();
        private String XmlPath = CurrentDirectory + "Settings.xml";

        private Settings()
        {  
            if (!File.Exists(XmlPath))
            {
                XmlWriter textWritter = XmlWriter.Create(XmlPath);
                textWritter.WriteStartDocument(true);
                textWritter.WriteStartElement("Settings");
                textWritter.WriteEndElement();
                textWritter.WriteEndDocument();
                textWritter.Close();
            }
            document.Load(XmlPath);
        }

        public static Settings GetInstance()
        {
            if (instance == null)
            {
                instance = new Settings();
            }
            return instance;
        }

        public String GetOne(String key, String path = "//*")
        {
            XmlNode Result = document.SelectSingleNode(path + "[@key ='" + key + "']");
            return Result.Attributes.GetNamedItem("Value").ToString();
        }

        public bool SetOne(String key, String value, String parent="/Settings")
        {
            // finds root node, appends new element to it, then append attributes key and value with received values
            XmlNode Parent = document.SelectSingleNode(parent);
            XmlNode newNode = document.CreateElement("Settings");
            XmlAttribute Key = document.CreateAttribute("Key");
            XmlAttribute Value = document.CreateAttribute("Value");
            Key.Value = key;
            Value.Value = value;
            Parent.AppendChild(newNode);
            newNode.Attributes.Append(Key);
            newNode.Attributes.Append(Value);
            try
            {
                document.Save(XmlPath);
                return true;
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
                return false;
            }
        }

        public XmlNodeList GetAll(String key, String path = "//*")
        {
            XmlNodeList Result = document.SelectNodes(path + "[@key ='" + key + "']");
            return Result;

        }

        public bool CreateChildNode(String name, String parent)
        {
            XmlNode Parent = document.SelectSingleNode(parent);
            XmlNode newNode = document.CreateElement(name);
            Parent.AppendChild(newNode);
            try
            {
                document.Save(XmlPath);
                return true;
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
                return false;
            }
        }
    }
}
